const {series} = require('gulp');
const gulp = require('gulp'),
      sass = require('gulp-sass'),
      browserSync = require('browser-sync').create();

function compileScss(){
    return gulp.src('./css/scss/*.scss')
           .pipe(sass())
           .pipe(gulp.dest('./css/output/'))
           .pipe(browserSync.stream());
}

function toSync(){
    browserSync.init({
        server:{
            baseDir: './'
        }
    });
    gulp.watch('./css/scss/*.scss',compileScss);
    gulp.watch('./*.html').on('change',browserSync.reload);
}

exports.default = series(compileScss,toSync);



